## Run automated QA against the staging instance

After each deploy to staging, the automated QA tests should be run against staging to verify core functionality.

### Required information

* The current stable branch, we'll refer to this as STABLE_BRANCH
* The qa username and password from the `Team` vault in 1 password. This is under `GitLab QA`. We will refer to these as QA_USERNAME and QA_PASSWORD

### Running the tests

From the `qa` subdirectory of a `gitlab-ee` clone.

1. Ensure you are on the stable branch for the release: `git checkout STABLE_BRANCH`
1. Ensure you are up to date: `git pull`
1. Ensure the correct dependencies are installed: `bundle install`
1. Run `GITLAB_USERNAME=${QA_USERNAME} GITLAB_PASSWORD=${GITLAB_PASSWORD} bin/qa Test::Instance https://staging.gitlab.com`
1. Save the test results in the `Automated QA` section of the `QA issue`.
  * If there are errors, create an issue and name it `Automation Triage RELEASE_MAJOR_VERSION RC#` and link it to the `QA issue`. 
  * In the triage issue include all the detailed test logs and screenshots.
1. For test automation failures:
  * If the failure is deemed a flaky test or an issue with the test framework, open an issue to fix this in the [gitlab-ce](https://gitlab.com/gitlab-org/gitlab-ce) project and add the ~QA label. 
  * If the failure is a defect, help track down the issue. Involve other developers as needed and raise the problem to Release Managers.
