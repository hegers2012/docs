# Security engineer in security releases

## General process

There are two types of security releases: [critical](#critical) and
[non-critical](#non-critical). The main difference is in who initiates the
release process. Critical security releases are initiated on-demand by the
security team to resolve the most severe vulnerabilities that are labelled as
`S1`. They are highly urgent and need to be released as soon as the patch is
ready. Non-critical security releases are scheduled monthly and are initiated
by the release management team. These contain lower severity and priority
issues.


### Non-critical

The Security Engineer is responsible for opening a new Security Release Issue
after the completion of the previous non-critical security release.  The
security release manager should be identified and assigned to the release.

Once a fix has been provided for a security issue:
1. Request a pre-assigned CVE using the [webform](https://cveform.mitre.org/).
1. Add it to the next security release issue so that it can be backported and released.


Before 23:59 PT on the 24th:
1. Create a WIP blog post MR in dev.gitlab.org. Ensure to add the CVE IDs, vulnerability descriptions, affected versions, remediation info, and a thank you to the reporters. Much of this information can be copy and pasted from the [summary table](https://dev.gitlab.org/gitlab/gitlabhq/blob/master/.gitlab/issue_templates/Security%20Developer%20Workflow.md#summary) in the developer security issue created on `dev.gitlab.org`. Also, feel free to use a past security release as a guide, for example [this blog post](https://about.gitlab.com/2018/04/04/security-release-gitlab-10-dot-6-dot-3-released/).
1. Assign the blog post to the RM for review.
1. Create a WIP issue in https://gitlab.com/gitlab-com/marketing/general and request an email notification be sent to subscribers of the `Security Alert` segment on the day of the release. Include a note that this should be sent out **after** the blog post is live. Also mention that you'll include the link to the blog post MR once it is prepared. The content of this notification should be similar to the blog post introduction:

>"Today we are releasing versions X.X.X, X.X.X, and X.X.X for GitLab Community Edition (CE) and Enterprise Edition (EE).

>These versions contain a number of important security fixes, and we strongly recommend that all GitLab installations be upgraded to one of these versions immediately. Please forward this alert to appropriate people at your organization and have them subscribe to [Security Notices](https://about.gitlab.com/contact/).

>For more details about this release, please visit our [blog](TODO)."

#### Blog post

Once blog post on dev.gitlab.org has been reviewed and is ready:
1. Either close or merge the dev.gitlab.org MR.
1. Sync the dev.gitlab.org `www-gitlab-com` repo with www.gitlab.com one. This creates the same blog post MR in www.gitlab.com.
1. Assign it to the RM for one last review.
1. Put the link of the new blog post in the email notification request issue as well as the security release issue.
1. Provide comments on all three. (old dev MR, new gitlab.com MR, and the email notification).

Quality Engineer notifies Security Engineer that the testing environments are ready for testing. Once the `QA issue` is created, Security Engineer, needs to:
1. Assign yourself to the issue. This specifies that you are working on verifying the fix.
1. Validate the fixes. The testing is typically performed on `staging.gitlab.com` environment.
1. Once validated, assign the issue back to the release manager(s) and notify them that the fixes are ready to be published.

Once packages are published:
1. Merge the gitlab.com blog post to make it live.
1. Mention in the email notification issue that it should be made live.
1. Follow-up with disclosure reports to inform/thank security researchers and HackerOne issues. Close out the issues on both HackerOne and Gitlab.
1. Create a new issue for the next scheduled security release and begin adding issues which are ready to be backported and released.


30 days after the release is published:
1. Remove confidentiality from the security issues.
1. Update the CVE IDs to public. `Notify CVE about a publication` on the [webform](https://cveform.mitre.org/).

### Critical

Once a fix has been provided for a critical `S1` labelled security issue:
1. Request a pre-assigned CVE using the [webform](https://cveform.mitre.org/).
1. Create a critical security release issue so that it can be backported and released immediately.
1. Notify the RM team to prepare for the critical security release as soon as possible.
1. Create a WIP blog post MR in dev.gitlab.org. Ensure to add the CVE IDs, vulnerability descriptions, affected versions, remediation info, and a thank you to the reporter. Much of this information can be copy and pasted from the [summary table](https://dev.gitlab.org/gitlab/gitlabhq/blob/master/.gitlab/issue_templates/Security%20Developer%20Workflow.md#summary) in the developer security issue created on `dev.gitlab.org`. Also, feel free to use a past security release as a guide, for example [this blog post](https://about.gitlab.com/2018/04/04/security-release-gitlab-10-dot-6-dot-3-released/).
1. Assign the blog post to the RM for review.
1. Create a WIP issue in https://gitlab.com/gitlab-com/marketing/general and request an email notification be sent to subscribers of the `Security Alert` segment on the day of the release. Include a note that this should be sent out **after** the blog post is live. Also mention that you'll include the link to the blog post MR once it is prepared. The content of this notification should be similar to the blog post introduction:

>"Today we are releasing versions X.X.X, X.X.X, and X.X.X for GitLab Community Edition (CE) and Enterprise Edition (EE).

>These versions contain a number of important security fixes, and we strongly recommend that all GitLab installations be upgraded to one of these versions immediately. Please forward this alert to appropriate people at your organization and have them subscribe to [Security Notices](https://about.gitlab.com/contact/).

>For more details about this release, please visit our [blog](TODO)."

#### Blog post

Once blog post on dev.gitlab.org has been reviewed and is ready:
1. Either close or merge the dev.gitlab.org MR.
1. Sync the dev.gitlab.org `www-gitlab-com` repo with www.gitlab.com one. This creates the same blog post MR in www.gitlab.com.
1. Assign it to the RM for one last review.
1. Put the link of the new blog post in the email notification request issue as well as the security release issue.
1. Provide comments on all three. (old dev MR, new gitlab.com MR, and the email notification).

Quality Engineer notifies Security Engineer that the testing environments are ready for testing. Once the `QA issue` is created, Security Engineer, needs to:
1. Assign yourself to the issue. This specifies that you are working on verifying the fix.
1. Validate the fixes. The testing is typically performed on `staging.gitlab.com` environment.
1. Once validated, assign the issue back to the release manager(s) and notify them that the fixes are ready to be published.

Once packages are published:
1. Merge the gitlab.com blog post to make it live.
1. Mention in the email notification issue that it should be made live.
1. Follow-up with disclosure reports to inform/thank security researchers and HackerOne issues. Close out the issues on both HackerOne and Gitlab.
1. Create a new issue for the next scheduled security release and begin adding issues which are ready to be backported and released.


30 days after the release is published:
1. Remove confidentiality from the security issues.
1. Update the CVE IDs to public. `Notify CVE about a publication` on the [webform](https://cveform.mitre.org/).
