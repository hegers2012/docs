# Security Releases general information

Security vulnerabilities in GitLab and its dependencies are to be addressed with
the highest priority.

Security releases are naturally very similar to [patch releases](../patch/process.md), but
on a much shorter timeline. The goal is to make a security release available as
soon as possible, while ensuring that the security issue is properly addressed
and that the fix does not introduce regressions.

At GitLab, we have two types of security releases:

 * [critical](#critical-security-release): immediate patch and mitigation is required for a single issue
 * [non-critical](#non-critical-security-releases): monthly release of planned issue

Also see [the GitLab Maintenance Policy](https://docs.gitlab.com/ee/policy/maintenance.html)
for the information on determining supported releases and assigning versions.

# General process overview

Release manager process is [described here](release-manager.md). The release
manager also makes sure that all the deadlines (described below) are
respected. Any delay needs to be escalated to Director of Backend and
Security.

Security engineers need to follow the process [defined here](security-engineer.md).

Developers need to follow the the process [defined here](developer.md).

Quality engineers need to follow the process [defined here](quality.md).

# Non-critical Security Releases

Every 28th of the month, we are releasing patch versions of GitLab containing
non-critical security fixes. This date has been chosen as it gives enough
time to release regular patch releases after standard release on the 22nd of
the month. This date also allows us to create and test fixes internally as
the next development cycle (starting on the 8th) starts.

## Release deadlines

Anything that can be carried out before the deadline, should be done.
Anything that is not done by the deadline requires immediate escalation to
people responsible of security release process.
Dates in this section can be considered as non-negotiable deadlines.

Non-critical security release contains security fixes that are ready for merge
**by 23:59 Pacific time on the 23rd** of the month. Both the fixes for the
current release and *all* of the backports need to be ready for merge and have
green tests.

If fixes and backports are not ready by that date, they will
have to go into the next security release.
**Exceptions can't be made** because verifying and creating security releases
are time consuming and any delay puts regular releases in danger.

Current release and all backports need to be tagged and packages created
**by 23:59 PT on the 24th**. This is also the deadline for creating the initial
blog post.

Once the packages are ready and up until **23:59 Pacific time on the 26th** of the
month, Quality Assurance tasks need to be carried out on *all* created releases to
verify that the fixes are indeed working as intended. During this period,
the tests should be executed on separate infrastructure created to carry out these tests.
Staging/canary/production are should remain open for regular releases.

Deploy to staging and canary environments should be carried out by **23:59 Pacific time
on the 27th** the latest.
Deploy to production has to be completed by **07:00 Pacific time on the 28th**.
Package promotion has to be coordinated with the blog post release. 1 hour before
the scheduled blog post announcement, package promotion should be done. This is
required because package promotion takes time to propagate.

Blog post, tweet and the email announcement should be complete by **10:00 Pacific time on the 28th**.

# Critical Security Releases

Depending on the severity and the attack surface of the vulnerability, an
immediate patch release consisting of just the security fix may be warranted.

The critical release process follows most of the steps of a non-critical release
with the following major differences:
 * Depending on the nature of the issue, mitigations may be able to be applied
   to the affected environment immediately.
 * A Security Team member is assigned to determine the affected users and any
   further required investigation.
 * The produced patch releases will address a single issue.
 * Schedule is assumed to be roughly 1 week, but this varies by vulnerability and
   the number of releases affected.
 * Communication is a priority for critical releases.  There will be three blog posts
   and a notification email:
   * First blog post pre-announces the patch date 
   * Second blog post is published when the patch is released, and has high-level information about the fixes
   * At the time of the second blog post, notification emails are sent to affected users if any
   * Third blog post is published approximately 30 days after the patch is released, and has full details of
     the vulnerability

This [critical release checklist](critical.md) can be used as a
guide for activities that need to be performed in the week preceding a
critical security release. This is intended to be a template and suggested
schedule for actions required for GitLab to respond to critical
vulnerabilities. Steps include actions taken from the moment the
vulnerability is discovered/reported up to the final package deployments and
announcements.

# Guides by role

- [Release Manager](release-manager.md)
- [Security Engineer](security-engineer.md)
- [Developer](developer.md)
- [Quality Engineer](quality.md)

---

[Return to Guides](../README.md)
