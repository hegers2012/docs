# QA issue generation

Manual QA is performed on changes that have been introduced between versions.

The QA generation task can be used to create an issue that lists the Merge Requests associated with merge commits introduced between two references.

For example:
- A QA issue for a patch release would list changes between tags `v10.8.1` and `v10.8.2`
- A QA issue for a new monthly release would list changes between the previous release's stable branch `10-8-stable` and the latest RCs tag `v11.0.0-rc1` 

> Note: You can verify the output of the generated QA task by doing a dry-run using `TEST=true`. Please see the [Rake task docs](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#qa_issuefromtoversion)

## First Release Task

### 1. Create an issue to track the QA tasks

To ensure that manual QA for changes is carried out we create a task which generates a checklist of changes

1. Make sure you have [release-tools](https://gitlab.com/gitlab-org/release-tools) cloned locally, and [set it up](https://gitlab.com/gitlab-org/release-tools/blob/master/doc/rake-tasks.md#setup)
1. Create the issue using the Rake task in the `release-tools` directory:

  - 1st Argument: from `ref` (sha, branch or tag)
  - 2nd Argument: to `ref` (sha, branch or tag)
  - 3rd Argument: release version to be used for the issue title

    ```sh
    # NOTE: This command is an example! Update it to reflect new version numbers.
    bundle exec rake "qa_issue[10-8-stable,v11.0.0-rc1,v11.0.0-rc1]"
    ```

## Subsequent release tasks

### 1. Update the current QA task

Follow the above steps to create a new issue. A new issue should be created for each RC

[Return to Guides](../README.md#guides)
